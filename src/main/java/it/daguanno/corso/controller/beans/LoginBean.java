package it.daguanno.corso.controller.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@ManagedBean(name="loginBeanMail")
//@Component("loginBeanMail")
//@Scope(value = "view")
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String loggato(){
		return "daniele/mailpage";
	}
	
}
