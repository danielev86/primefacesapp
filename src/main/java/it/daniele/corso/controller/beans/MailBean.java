package it.daniele.corso.controller.beans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.daniele.corso.service.beans.Mail;

@ManagedBean
@Component
@Scope(value = "session")
public class MailBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<Mail> lstRicevute;
	private List<Mail> lstSpam;
	private List<String> lstSocial;
	private List<Mail> lstInviate;
	private boolean inviateCheck;
	private boolean ricevuteCheck;
	private String destinatario;
	private String contenuto;
	private List<String> lstChatMessage;
	private String chatMessage;
	private Mail mailSelected;
	
	private int contatore;

	@Autowired
	private LoginBeanMail loginBeanMail;

	@PostConstruct
	public void init() throws ParseException {
		contatore = 0;
		lstInviate = new ArrayList<Mail>();
		lstRicevute = new ArrayList<Mail>();
		Mail mail1 = new Mail();
		mail1.setFlag(false);
		mail1.setIndirizzo("pippo@exaxmple.com");
		mail1.setTesto("Ciao");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		mail1.setDataRicezione(sdf.parse("02/12/2015"));
		Mail mail2 = new Mail();
		mail2.setFlag(false);
		mail2.setIndirizzo("pluto@exaxmple.com");
		mail2.setTesto("Ciao");
		mail2.setDataRicezione(sdf.parse("02/12/2014"));

		Mail mail3 = new Mail();
		mail3.setFlag(false);
		mail3.setIndirizzo("paperino@exaxmple.com");
		mail3.setTesto("Ciao");
		mail3.setDataRicezione(sdf.parse("02/12/2013"));

		lstRicevute.add(mail1);
		lstRicevute.add(mail2);
		lstRicevute.add(mail3);

		lstSpam = new ArrayList<Mail>();
		Mail mailS1 = new Mail();
		mailS1.setIndirizzo("calda@exaxmple.com");
		mailS1.setTesto("Ciao bonazzo");
		Mail mailS2 = new Mail();
		mailS2.setIndirizzo("xxxx@exaxmple.com");
		mailS2.setTesto("XXXXXXXX");
		Mail mailS3 = new Mail();
		mailS3.setIndirizzo("papo@xxx.com");
		mailS3.setTesto("Poste italiane fake");
		lstSpam.add(mailS1);
		lstSpam.add(mailS2);
		lstSpam.add(mailS3);

		lstSocial = new ArrayList<String>();
		lstSocial.add("Sei stato contatto da Roberta XXXX");
		lstSocial.add("Roberta XXXX ha richiesto la tua amicizia");
		lstSocial.add("Frank ha richiesto la tua amicizia");
		lstSocial.add("Helen ti vuol uccidere");

		lstChatMessage = new ArrayList<String>();

	}

	public Mail getMailSelected() {
		return mailSelected;
	}

	public void setMailSelected(Mail mailSelected) {
		this.mailSelected = mailSelected;
	}

	public String getChatMessage() {
		return chatMessage;
	}

	public void setChatMessage(String chatMessage) {
		this.chatMessage = chatMessage;
	}

	public List<String> getLstChatMessage() {
		return lstChatMessage;
	}

	public void setLstChatMessage(List<String> lstChatMessage) {
		this.lstChatMessage = lstChatMessage;
	}

	public List<Mail> getLstRicevute() {
		return lstRicevute;
	}

	public void setLstRicevute(List<Mail> lstRicevute) {
		this.lstRicevute = lstRicevute;
	}

	public List<Mail> getLstSpam() {
		return lstSpam;
	}

	public void setLstSpam(List<Mail> lstSpam) {
		this.lstSpam = lstSpam;
	}

	public List<String> getLstSocial() {
		return lstSocial;
	}

	public void setLstSocial(List<String> lstSocial) {
		this.lstSocial = lstSocial;
	}

	public List<Mail> getLstInviate() {
		return lstInviate;
	}

	public void setLstInviate(List<Mail> lstInviate) {
		this.lstInviate = lstInviate;
	}

	public boolean isInviateCheck() {
		return inviateCheck;
	}

	public void setInviateCheck(boolean inviateCheck) {
		this.inviateCheck = inviateCheck;
	}

	public boolean isRicevuteCheck() {
		return ricevuteCheck;
	}

	public void setRicevuteCheck(boolean ricevuteCheck) {
		this.ricevuteCheck = ricevuteCheck;
	}

	public void ricevute() {
		inviateCheck = false;
		ricevuteCheck = true;
	}

	public void inviate() {
		inviateCheck = true;
		ricevuteCheck = false;
	}

	public void send() {
		RequestContext.getCurrentInstance().execute("PF('wgDialogMail').show();");

	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getContenuto() {
		return contenuto;
	}

	public void setContenuto(String contenuto) {
		this.contenuto = contenuto;
	}

	public void sendMail() {
		Mail mail = new Mail();
		mail.setIndirizzo(destinatario);
		mail.setTesto(contenuto);
		lstInviate.add(mail);
		destinatario = "";
		contenuto = "";
		RequestContext.getCurrentInstance().execute("PF('wgDialogMail').hide();");
		inviateCheck = true;
		ricevuteCheck = false;

	}

	public void sendMessageChat() throws InterruptedException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		this.lstChatMessage.add(loginBeanMail.getUsername() + " " + sdf.format(new Date()) + ">>>" + chatMessage);
		Thread.sleep(1500L);
		if(chatMessage.toLowerCase().contains("Ciao".toLowerCase())){
			this.lstChatMessage.add("xxxxx " + " " + sdf.format(new Date()) + ">>>" + "Ciao stai parlando con un robot");

		}else if(chatMessage.toLowerCase().contains("Come stai".toLowerCase())){
			this.lstChatMessage.add("xxxxx " + " " + sdf.format(new Date()) + ">>>" + "Io sto bene e tu?");
		}else if(chatMessage.toLowerCase().contains("anni".toLowerCase())){
			this.lstChatMessage.add("xxxxx " + " " + sdf.format(new Date()) + ">>>" + "Io sono un robot non ho una data di nascita");
		}else{
			this.lstChatMessage.add("xxxxx " + " " + sdf.format(new Date()) + ">>>" + "Non sono in grado di rispondere alla tua domanda");

		}

		this.chatMessage = "";
	}

	public void showChat() {
		lstChatMessage = new ArrayList<String>();
		RequestContext.getCurrentInstance().execute("PF('wgChat').show();");
	}

	public void closeChat() {
		RequestContext.getCurrentInstance().execute("PF('wgChat').hide();");

	}

	public void openMail(ActionEvent e) {
		mailSelected = new Mail();
		String indirizzo = e.getComponent().getAttributes().get("indirizzoDest").toString();
		String mail = e.getComponent().getAttributes().get("testoDest").toString();
		mailSelected.setIndirizzo(indirizzo);
		mailSelected.setTesto(mail);
		RequestContext.getCurrentInstance().execute("PF('wgMailView').show();");
	}

	public void remove(Mail mail) {
		Mail mailTmp = new Mail();
		mailTmp.setTesto(mail.getTesto());
		mailTmp.setIndirizzo(mail.getIndirizzo());
		// mailTmp.setFlag(!mail.is);
	}

	public void ordinaPriorita(Mail mail) {
		if (mail.isFlag()) {
			lstRicevute.remove(mail);
			lstRicevute.add(0, mail);
		}else{
			lstRicevute.remove(mail);
			lstRicevute.add(mail);
		}
	}
	
	public void socialAdd(){
		
		lstSocial.add("XXX"+contatore+" ti ha aggiunto nella lista degli amici");
	}

}
