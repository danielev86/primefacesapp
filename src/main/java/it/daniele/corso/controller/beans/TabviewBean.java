package it.daniele.corso.controller.beans;

import javax.faces.bean.ManagedBean;

import org.primefaces.event.TabChangeEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@ManagedBean
@Component
@Scope(value="view")
public class TabviewBean {
	
	public void change(TabChangeEvent event){
		
		   if (event.getTab().getId().equals("nibali")) {
			   System.out.println("Stampa messaggio nibali");
		   } else if(event.getTab().getId().equals("aru")) {
			   System.out.println("Stampa messaggio aru");
		   }
		
	}

}
