package it.daniele.corso.controller.beans;

import java.io.Serializable;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@ManagedBean
@Component
@Scope(value = "session")
public class PopupBean implements Serializable {

	private String[] arrayUrl;
	private String url;
	private int count;

	@PostConstruct
	public void init() {
		arrayUrl = new String[5];
		arrayUrl[0] = "http://design.ubuntu.com/wp-content/uploads/logo-ubuntu_su-orange-hex.png";
		arrayUrl[1] = "https://vmdepotwestus.blob.core.windows.net/images/17/661f299e-b9bb-4a2c-9e57-aa603de02003_215x215.png";
		arrayUrl[2] = "http://thevarguy.com/site-files/thevarguy.com/files/archive/thevarguy.com/wp-content/uploads/2008/12/ibm-and-canonical-attack-windows.jpg";
		arrayUrl[3] = "http://www.laptopmag.com/images/wp/purch-api/incontent/2012/04/EndOfWorld.jpg";
		arrayUrl[4] = "http://www.drbill.tv/images/RIP-Windows.png";
		int random = new Random().nextInt(4);
		this.url = arrayUrl[random];
		count = 0;

	}

	public String[] getArrayUrl() {
		return arrayUrl;
	}

	public void setArrayUrl(String[] arrayUrl) {
		this.arrayUrl = arrayUrl;
	}

	public String getUrl() {
		if(count==4){
			count=0;
			this.url = arrayUrl[count];
			count++;
		}else{
			count++;
			this.url = arrayUrl[count];
		}

		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}



	public void aggiornaPannello() {
		int random = new Random().nextInt(4);
		this.url = arrayUrl[random];
	}

}
