package it.daniele.corso.controller.beans;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@ManagedBean(name = "pollBean")
@Component("pollBean")
@Scope(value = "view")
public class PollBean {

	@Autowired
	private TabellaRefreshBean tabellaRefreshBean;
	private String field;
	private int number;
	private String username;
	private boolean viewPanel;


	public boolean isViewPanel() {
		return viewPanel;
	}

	public void setViewPanel(boolean viewPanel) {
		this.viewPanel = viewPanel;
	}

	public TabellaRefreshBean getTabellaRefreshBean() {
		return tabellaRefreshBean;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setTabellaRefreshBean(TabellaRefreshBean tabellaRefreshBean) {
		this.tabellaRefreshBean = tabellaRefreshBean;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public int getNumber() {
		return number;
	}

	public void increment() {
		number++;
	}

	public void aggiungiItem() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy  HH:mm");
		tabellaRefreshBean.addElement(username + " " + sdf.format(new Date()) + " >>> " + field);
		this.field = "";
	}

	public void showPanel() {
		this.viewPanel = true;
		RequestContext.getCurrentInstance().execute("PF('wgPanel').show();");

		
		// field = null;
		// UIComponent panel
		// =FacesContext.getCurrentInstance().getViewRoot().findComponent("widgetPanelLogin");
		// panel.setRendered(false);
	}
	
	public void mostra(){
		RequestContext.getCurrentInstance().execute("PF('wgPanelLogin').show()");
	}
	
	public void nascondi(){
		RequestContext.getCurrentInstance().execute("PF('wgPanelLogin').close()");
		

	}

}
