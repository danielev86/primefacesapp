package it.daniele.corso.controller.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@ManagedBean(name="addBean")
@Component("addBean")
@Scope(value="view")
public class AddTabBean {
	
	private String item;
	private List<String> lstItems;
	@PostConstruct
	public void init(){
		lstItems = new ArrayList<String>();
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public List<String> getLstItems() {
		return lstItems;
	}
	public void setLstItems(List<String> lstItems) {
		this.lstItems = lstItems;
	}
	
	public void addInsert(){
		this.lstItems.add(item);
		item="";
		stampa();
	}
	
	public void stampa(){
		for(String it:lstItems){
			System.out.println(it);
		}
	}
	
	

}
