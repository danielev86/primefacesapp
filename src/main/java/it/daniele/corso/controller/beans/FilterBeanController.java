package it.daniele.corso.controller.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.daniele.corso.service.beans.Utente;

@ManagedBean(name="filterData")
@Component("filterData")
@Scope(value="session")
public class FilterBeanController {
	private Date dataDa;
	private Date dataA;
	private List<Utente> lstFilter;
	@Autowired
	private UtenteControllerBean utenteControllerBean;
	private Utente utenteSelected;
	private Utente user;

	@PostConstruct
	public void init() {
		lstFilter = utenteControllerBean.getLstUtenti();

	}
	
	
	
	

	public Utente getUser() {
		return user;
	}





	public void setUser(Utente user) {
		this.user = user;
	}





	public Utente getUtenteSelected() {
		return utenteSelected;
	}



	public void setUtenteSelected(Utente utenteSelected) {
		this.utenteSelected = utenteSelected;
	}



	public Date getDataDa() {
		return dataDa;
	}

	public void setDataDa(Date dataDa) {
		this.dataDa = dataDa;
	}

	public Date getDataA() {
		return dataA;
	}

	public void setDataA(Date dataA) {
		this.dataA = dataA;
	}

	public List<Utente> getLstFilter() {
		return lstFilter;
	}

	public void setLstFilter(List<Utente> lstFilter) {
		this.lstFilter = lstFilter;
	}

	public UtenteControllerBean getUtenteControllerBean() {
		return utenteControllerBean;
	}

	public void setUtenteControllerBean(UtenteControllerBean utenteControllerBean) {
		this.utenteControllerBean = utenteControllerBean;
	}
	
	public void filter(){
		lstFilter = new ArrayList<Utente>();
		for(Utente it:utenteControllerBean.getLstUtenti()){
			if(it.getData().before(dataA) && it.getData().after(dataDa)){
				lstFilter.add(it);
			}
		}
	}
	
	public void update(){
		lstFilter = utenteControllerBean.getLstUtenti();
		lstFilter.remove(user);
		lstFilter.add(utenteSelected);
		
	}
	
	
}
