package it.daniele.corso.controller.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@ManagedBean(name="searchBean")
@Component("searchBean")
@Scope(value="view")
public class SearchTabBean {
	
	@Autowired
	private AddTabBean addTabBean;
	private String search;
	private List<String> lstFilter;
	private boolean value;
	
	
	
	@PostConstruct
	public void init(){
		this.lstFilter = new ArrayList<String>();
	}
	
	
	public boolean isValue() {
		return value;
	}


	public void setValue(boolean value) {
		this.value = value;
	}


	public AddTabBean getAddTabBean() {
		return addTabBean;
	}
	public void setAddTabBean(AddTabBean addTabBean) {
		this.addTabBean = addTabBean;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	
	
	public List<String> getLstFilter() {
		return lstFilter;
	}
	public void setLstFilter(List<String> lstFilter) {
		this.lstFilter = lstFilter;
	}
	public void ricerca(){
		value=true;
		for(String it:addTabBean.getLstItems()){
			if(it.equals(search)){
				lstFilter.add(it);
			}
		}
		search="";
	}
	
	
	

}
