package it.daniele.corso.controller.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@ManagedBean(name = "tabRefresh")
@Component("tabRefresh")
@Scope(value = "application")
public class TabellaRefreshBean {

	private List<String> lstElements;
	
	@PostConstruct
	public void init(){
		this.lstElements = new ArrayList<String>();
	}

	public List<String> getLstElements() {
		return lstElements;
	}

	public void setLstElements(List<String> lstElements) {
		this.lstElements = lstElements;
		
	}

	public void addElement(String item) {
		this.lstElements.add(item);
		
	}

}
