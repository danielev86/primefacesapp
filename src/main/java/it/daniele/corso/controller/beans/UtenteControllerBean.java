package it.daniele.corso.controller.beans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.daniele.corso.service.beans.Utente;
@ManagedBean(name="utenteBean")
@Component("utenteBean")
@Scope(value="session")
public class UtenteControllerBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String cognome;
	private String indirizzo;
	private List<Utente> lstUtenti;

	private String sesso;
	private Date data;
	private String testo;

	

	public String getTesto() {
		return testo;
	}


	public void setTesto(String testo) {
		this.testo = testo;
	}


	@PostConstruct
	public void init() throws ParseException{
		lstUtenti = new ArrayList<Utente>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Utente utente = new Utente();
		utente.setNome("daniele");
		utente.setCognome("daniele");
		utente.setIndirizzo("via daniele");
		utente.setSesso("Maschio");
		utente.setData(new Date());
		lstUtenti.add(utente);
		Utente utente1 = new Utente();
		utente1.setNome("daniele1");
		utente1.setCognome("daniele1");
		utente1.setIndirizzo("via daniele1");
		utente1.setSesso("Maschio");
		utente1.setData(sdf.parse("01/12/1800"));
		lstUtenti.add(utente1);
		Utente utente2 = new Utente();
		utente2.setNome("daniele2");
		utente2.setCognome("daniele2");
		utente2.setIndirizzo("via daniele2");
		utente2.setSesso("Maschio");

		utente2.setData(sdf.parse("01/12/2000"));
		lstUtenti.add(utente2);
	}
	
	
	public List<String> complete(String query){
        List<String> results = new ArrayList<String>();
        for(Utente it:lstUtenti) {
            results.add(it.getNome());
        }
         
        return results;
	}
	



	public Date getData() {
		return data;
	}




	public void setData(Date data) {
		this.data = data;
	}




	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public List<Utente> getLstUtenti() {
		return lstUtenti;
	}
	public void setLstUtenti(List<Utente> lstUtenti) {
		this.lstUtenti = lstUtenti;
	}
	
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	

	public void aggiungi(){
		Utente utente = new Utente();
		utente.setNome(nome);
		utente.setCognome(cognome);
		utente.setIndirizzo(indirizzo);
		utente.setSesso(sesso);
		utente.setData(data);
		lstUtenti.add(utente);
		nome="";
		cognome="";
		indirizzo="";
		data=null;
		stampa();
	}
	

	
	public void stampa(){
		for(Utente it:lstUtenti){
			System.out.println(it.getCognome()+"  "+it.getIndirizzo());
		}
	}

}
